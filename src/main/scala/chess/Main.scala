package chess

import chess.board._
import chess.problem.ChessProblem

object Main {
  def main(args: Array[String]): Unit = {
    Config.configParser.parse(args, Config()) match {
      case Some(config) =>
        println("Calculating solutions...")
        val (solvingTime, results) = benchmarkRun {
          ChessProblem.solve(Board(config.boardWidth, config.boardHeight), config.pieces)
        }
        displayResults(config, results, solvingTime)
      case None =>
    }
  }

  def benchmarkRun[T](f: => T): (Long, T) = {
    val start = System.currentTimeMillis()
    val result = f
    val end = System.currentTimeMillis()
    (end - start, result)
  }

  def displayResults(config: Config, results: Set[Board], solvingTime: Long): Unit = {
    if (config.showResults) {
      results.toSeq.zipWithIndex.foreach { case (result, index) =>
        println(s"Solution ${index + 1}:${result.currentView}\n")
      }
    }
    println(s"Number of solutions: ${results.size}")
    println(s"Solving the problem took: $solvingTime ms")
  }
}
