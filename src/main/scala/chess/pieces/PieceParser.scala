package chess.pieces

object PieceParser {
  def parse(name: String): Either[String, Piece] =
    name.trim.capitalize match {
      case "K" => Right(King)
      case "Q" => Right(Queen)
      case "R" => Right(Rook)
      case "B" => Right(Bishop)
      case "N" => Right(Knight)
      case other => Left(s"Unrecognized piece: $other")
    }
}
