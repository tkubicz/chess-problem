package chess.pieces

import chess.board._

trait Piece {
  def attacks(board: Board, fieldIndex: FieldIndex): Seq[FieldIndex]

  def priority: Int
}

trait DiagonalAttack {
  protected def checkDiagonalAttack(board: Board, fieldIndex: FieldIndex, limit: Boolean = false): Seq[FieldIndex] = {

    val position = board.position(fieldIndex)

    def calculateDiagonal(side: Seq[Int]): Seq[(Int, Int)] = {
      side.foldLeft((position.row, position.row, Seq.empty[(Int, Int)])) {
        case ((upperRow, lowerRow, coords), currentColumn) =>
          val newUpperRow = upperRow - 1
          val newLowerRow = lowerRow + 1
          (newUpperRow, newLowerRow, coords :+ (newUpperRow, currentColumn) :+ (newLowerRow, currentColumn))
      }._3.filterNot {
        case (r, c) if r >= board.height || r < 0 || c >= board.height || c < 0 => true
        case _ => false
      }
    }

    val (leftLimit, rightLimit) = if (limit) (position.column - 1, position.column + 2) else (0, board.width)
    val leftSide = position.column - 1 to leftLimit by -1
    val rightSide = position.column + 1 until rightLimit

    (calculateDiagonal(leftSide) ++ calculateDiagonal(rightSide)).map(board.position)
  }
}

trait LineAttack {
  protected def checkLineAttack(board: Board, fieldIndex: FieldIndex, limit: Boolean = false): Seq[FieldIndex] = {
    val position = board.position(fieldIndex)

    if (limit) {
      Seq(position.copy(column = position.column - 1), position.copy(column = position.column + 1),
        position.copy(row = position.row - 1), position.copy(row = position.row + 1))
        .filterNot {
          case Position(r, c) if r >= board.height || r < 0 || c >= board.height || c < 0 => true
          case _ => false
        }.map(board.position)
    } else {
      ((0 until board.width).map(x => (x, position.column)) ++ (0 until board.height).map(y => (position.row, y)))
        .filterNot {
          case (x, y) => x == position.row && y == position.column
        }.map(board.position)
    }
  }
}

trait KnightAttack {
  protected def checkKnightAttack(board: Board, fieldIndex: FieldIndex): Seq[FieldIndex] = {
    val position = board.position(fieldIndex)
    Seq(
      (position.row - 1, position.column - 2), (position.row - 1, position.column + 2),
      (position.row + 1, position.column - 2), (position.row + 1, position.column + 2),
      (position.row - 2, position.column - 1), (position.row - 2, position.column + 1),
      (position.row + 2, position.column - 1), (position.row + 2, position.column + 1)
    ).filterNot {
      case (x, y) => (x < 0 || x >= board.width) || (y < 0 || y >= board.height)
    }.map(board.position)
  }
}

object Queen extends Piece with DiagonalAttack with LineAttack {
  override def attacks(board: Board, fieldIndex: FieldIndex): Seq[FieldIndex] =
    checkDiagonalAttack(board, fieldIndex) ++ checkLineAttack(board, fieldIndex)

  override def priority: Int = 1

  override def toString: String = "Q"
}

object Rook extends Piece with LineAttack {
  override def attacks(board: Board, fieldIndex: FieldIndex): Seq[FieldIndex] = checkLineAttack(board, fieldIndex)

  override def priority: Int = 2

  override def toString: String = "R"
}

object Bishop extends Piece with DiagonalAttack {
  override def attacks(board: Board, fieldIndex: FieldIndex): Seq[FieldIndex] = checkDiagonalAttack(board, fieldIndex)

  override def priority: Int = 3

  override def toString: String = "B"
}

object Knight extends Piece with KnightAttack {
  override def attacks(board: Board, fieldIndex: FieldIndex): Seq[FieldIndex] = checkKnightAttack(board, fieldIndex)

  override def priority: Int = 4

  override def toString: String = "N"
}

object King extends Piece with DiagonalAttack with LineAttack {
  override def attacks(board: Board, fieldIndex: FieldIndex): Seq[FieldIndex] =
    checkDiagonalAttack(board, fieldIndex, limit = true) ++ checkLineAttack(board, fieldIndex, limit = true)

  override def priority: Int = 5

  override def toString: String = "K"
}
