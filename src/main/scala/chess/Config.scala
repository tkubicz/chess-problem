package chess

import chess.pieces.{Piece, PieceParser}
import scopt.OptionParser

case class Config(boardWidth: Int = -1, boardHeight: Int = -1, pieces: Seq[Piece] = Seq.empty, showResults: Boolean = false)

object Config {
  val configParser: OptionParser[Config] = new scopt.OptionParser[Config]("chess-problem") {
    head("chess-problem", "0.1")

    opt[Int]('w', "width").required().valueName("<int>")
      .action { case (value, config) => config.copy(boardWidth = value) }
      .validate { value =>
        if (value > 1) success else failure("board width has to be bigger than 1")
      }
      .text("Width of the chess board")

    opt[Int]('h', "height").required().valueName("<int>")
      .action { case (value, config) => config.copy(boardHeight = value) }
      .validate { value =>
        if (value > 1) success else failure("board height has to be bigger than 1")
      }
      .text("Height of the chess board")

    opt[Seq[String]]('p', "pieces").required()
      .valueName("<Piece>,<Piece>...")
      .action {
        case (value, config) =>
          val validPieces = value.map(PieceParser.parse).collect { case Right(piece) => piece }
          config.copy(pieces = validPieces)
      }
      .validate { value =>
        val unrecognizedPieces = value.map(PieceParser.parse).collect { case Left(msg) => msg }
        if (unrecognizedPieces.isEmpty) success else failure(unrecognizedPieces.mkString("\n"))
      }
      .text("Possible values for <Piece>: K (King), Q (Queen), R (Rook), B (Bishop), N (Knight)")

    opt[Boolean]('r', "results").valueName("<bool>")
      .action { case (value, config) => config.copy(showResults = value) }
      .text("Set this flag to 'true' to display all solutions on standard output. Default is 'false'")
  }
}