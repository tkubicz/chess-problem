package chess.problem

import chess.board.{Board, FieldIndex}
import chess.pieces.Piece

object ChessProblem {

  // Working solution, but it's pretty slow. It requires almost 10 minutes to calculate 7x7 board.
  // The plus is, it's quite compact ;).
  def solveSlow(board: Board, pieces: Seq[Piece]): Set[Board] = {
    def solveSlowStep(boards: Set[Board], pieces: Seq[Piece]): Set[Board] = {
      pieces.headOption match {
        case None => boards
        case Some(piece) =>
          boards.flatMap(b => solveSlowStep(b.safeFields.flatMap(sf => b.putOnBoard(piece, sf)).toSet, pieces.tail))
      }
    }
    val orderedPieces = pieces.sortBy(_.priority)
    solveSlowStep(board.safeFields.flatMap(sf => board.putOnBoard(orderedPieces.head, sf)).toSet, orderedPieces.tail)
  }


  // Second solution, should be faster the the first one. It took about 155549 ~ 2.6 minutes to solve 7x7 board.
  def solve(board: Board, pieces: Seq[Piece]): Set[Board] = {
    def putPieces(pieces: Seq[Piece], accumulator: Set[Board]): Set[Board] = {
      pieces.headOption match {
        case None => accumulator
        case Some(piece) => putPieces(pieces.tail, putPiece(piece, accumulator, Set.empty[Board]))
      }
    }

    def putPiece(piece: Piece, boards: Set[Board], accumulator: Set[Board]): Set[Board] = {
      boards.headOption match {
        case None => accumulator
        case Some(b) =>
          putPiece(piece, boards.tail, accumulator ++ tryToPutPiece(b, piece))
      }
    }

    def tryToPutPiece(board: Board, piece: Piece): Set[Board] = {
      def putPieceOnPossibleSafeFields(fields: Seq[FieldIndex], accumulator: Set[Option[Board]]): Set[Option[Board]] = {
        fields.headOption match {
          case None => accumulator
          case Some(field) =>
            putPieceOnPossibleSafeFields(fields.tail, accumulator + board.putOnBoard(piece, field))
        }
      }

      putPieceOnPossibleSafeFields(board.safeFields, Set.empty[Option[Board]]).flatten
    }

    val orderedPieces = pieces.sortBy(_.priority)
    putPieces(orderedPieces, Set(board))
  }
}
