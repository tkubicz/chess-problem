package chess.board

import chess.pieces.Piece

sealed trait FieldState

case class Occupied(piece: Piece) extends FieldState {
  override def toString: String = piece.toString
}

case object Safe extends FieldState {
  override def toString: String = "_"
}

case object Dangerous extends FieldState {
  override def toString: String = "_"
}

case class FieldIndex(index: Int) extends AnyVal

object FieldIndex {
  implicit val fieldIndexOrdering: Ordering[FieldIndex] = Ordering.by(_.index)
}