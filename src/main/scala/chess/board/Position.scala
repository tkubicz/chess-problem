package chess.board

case class Position(row: Int, column: Int)
