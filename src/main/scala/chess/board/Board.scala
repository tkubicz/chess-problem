package chess.board

import chess.pieces.Piece

case class Board(width: Int, height: Int, fields: Map[FieldIndex, FieldState]) {

  // Converts 2D coordinates into 1D index
  def position(row: Int, col: Int): FieldIndex = FieldIndex(width * row + col)

  def position(coords: (Int, Int)): FieldIndex = position(coords._1, coords._2)

  def position(pos: Position): FieldIndex = position(pos.row, pos.column)

  // Converts 1D index into 2D coordinates
  def position(fi: FieldIndex): Position = Position(fi.index / width, fi.index % width)

  // Returns the current board view.
  def currentView: String =
    (0 until width * height)
      .foldLeft("") { case (acc, index) => s"$acc ${if (index % width == 0) "\n" else ""}  ${fields(FieldIndex(index))}" }

  def canPutOnBoard(piece: Piece, fieldIndex: FieldIndex): Boolean = {
    val attacks = piece.attacks(this, fieldIndex)
    safeFields.contains(fieldIndex) && !attacks.map(i => fields(i)).exists(_.isInstanceOf[Occupied])
  }

  def putOnBoard(piece: Piece, fieldIndex: FieldIndex): Option[Board] = {
    if (canPutOnBoard(piece, fieldIndex)) {
      val updatedBoard = piece
        .attacks(this, fieldIndex)
        .foldLeft(updateFieldState(fieldIndex, Occupied(piece))) {
          case (board, currentIndex) => board.updateFieldState(currentIndex, Dangerous)
        }
      Some(updatedBoard)
    } else None
  }

  def safeFields: Seq[FieldIndex] = fields.filter {
    case (_, state) if state == Safe => true
    case _ => false
  }.keys.toSeq.sorted

  def occupiedFields: Seq[FieldIndex] = fields.filter {
    case (_, state) if state.isInstanceOf[Occupied] => true
    case _ => false
  }.keys.toSeq.sorted

  private def updateFieldState(index: FieldIndex, fieldState: FieldState): Board =
    this.copy(fields = fields.updated(index, fieldState))
}

object Board {
  def apply(width: Int, height: Int): Board =
    Board(width, height, Seq.tabulate(width * height)(i => (FieldIndex(i), Safe)).toMap)
}
