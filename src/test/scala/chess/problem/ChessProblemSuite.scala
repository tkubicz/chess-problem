package chess.problem

import chess.board.Board
import chess.pieces.{King, Knight, Rook}
import org.scalatest.{FunSuite, Matchers}

class ChessProblemSuite extends FunSuite with Matchers {
  test("Board 3x3 with 2 Kings and 1 Rook should have 4 solutions (using first approach)") {
    ChessProblem.solveSlow(Board(3, 3), Seq(King, King, Rook)) should have size 4
  }

  test("Board 4x4 with 2 Rooks and 4 Knights should have 8 solutions (using first approach)") {
    ChessProblem.solveSlow(Board(4, 4), Seq(Rook, Rook, Knight, Knight, Knight, Knight)) should have size 8
  }

  test("Board 3x3 with 2 Kings and 1 Rook should have 4 solutions (using second approach)") {
    ChessProblem.solve(Board(3, 3), Seq(King, King, Rook)) should have size 4
  }

  test("Board 4x4 with 2 Rooks and 4 Knights should have 8 solutions (using second approach)") {
    ChessProblem.solve(Board(4, 4), Seq(Rook, Rook, Knight, Knight, Knight, Knight)) should have size 8
  }
}
