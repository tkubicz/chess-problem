package chess.board

import chess.pieces.{Bishop, Knight, Rook}
import org.scalatest._

class BoardSuit extends FunSuite with Matchers {

  test("position(row, col) should convert 2D coords into 1D coords") {
    val board = Board(4, 4)
    board.position(0, 0).index shouldEqual 0
    board.position(2, 3).index shouldEqual 11
    board.position(3, 1).index shouldEqual 13

    val secondBoard = Board(7, 7)
    secondBoard.position(0, 0).index shouldEqual 0
    secondBoard.position(0, 5).index shouldEqual 5
    secondBoard.position(3, 3).index shouldEqual 24
  }

  test("position(fieldIndex) should convert 1D coords into 2D coords") {
    val board = Board(4, 4)
    board.position(FieldIndex(0)) shouldEqual Position(0, 0)
    board.position(FieldIndex(15)) shouldEqual Position(3, 3)
    board.position(FieldIndex(10)) shouldEqual Position(2, 2)

    val secondBoard = Board(7, 7)
    secondBoard.position(FieldIndex(30)) shouldEqual Position(4, 2)
    secondBoard.position(FieldIndex(40)) shouldEqual Position(5, 5)
  }

  test("currentView() should return the string containing the current board view") {
    val board = Board(4, 4).putOnBoard(Rook, FieldIndex(0))
      .flatMap(_.putOnBoard(Bishop, FieldIndex(6))
        .flatMap(_.putOnBoard(Rook, FieldIndex(15))))

    board shouldBe defined
    board.get.currentView should not be empty
    // TODO: Check if the view is valid
  }

  test("putOnBoard() should return updated board") {
    val board = for {
      initial <- Some(Board(4, 4))
      a <- initial.putOnBoard(Rook, FieldIndex(0))
      b <- a.putOnBoard(Knight, FieldIndex(15))
    } yield b

    board shouldBe defined
    board.map(b => b.occupiedFields.size) shouldBe Some(2)
  }

  test("putOnBoard() should return None if piece cannot be placed in requested position") {
    val board0 = for {
      initial <- Some(Board(4, 4))
      a <- initial.putOnBoard(Rook, FieldIndex(0))
      b <- a.putOnBoard(Bishop, FieldIndex(12)) // Bishop cannot be placed here, because field is attacked by rook
    } yield b

    board0 shouldBe None

    val board1 = for {
      initial <- Some(Board(4, 4))
      a <- initial.putOnBoard(Rook, FieldIndex(0))
      b <- a.putOnBoard(Bishop, FieldIndex(5)) // Bishop cannot be placed here, because it would attack rook.
    } yield b

    board1 shouldBe None
  }

  test("It should be possible to place piece on any field on empty board") {
    val board = Board(4, 4)
    val result = (0 until 4 * 4).map { i =>
      board.canPutOnBoard(Rook, FieldIndex(i))
    }
    assert(result.forall(_ == true))
  }

  test("safeFields should return all safe fields on the board") {
    val board = Board(4, 4).putOnBoard(Rook, FieldIndex(0))
      .flatMap(_.putOnBoard(Bishop, FieldIndex(6))
        .flatMap(_.putOnBoard(Rook, FieldIndex(15))))

    board shouldBe defined
    board.map(_.safeFields.size).getOrElse(0) shouldEqual 2
    board.map(_.safeFields).getOrElse(Seq.empty) should contain only(FieldIndex(5), FieldIndex(10))
  }

  test("occupiedFields should return all occupied fields on the board") {
    val board = Board(4, 4).putOnBoard(Rook, FieldIndex(0))
      .flatMap(_.putOnBoard(Bishop, FieldIndex(6))
        .flatMap(_.putOnBoard(Rook, FieldIndex(15))))

    board shouldBe defined
    board.map(_.occupiedFields.size).getOrElse(0) shouldEqual 3
    board.map(_.occupiedFields.map(_.index)).getOrElse(Seq.empty) should contain only(0, 6, 15)
  }
}
