package chess.pieces

import chess.board.{Board, FieldIndex}
import org.scalatest._

class PiecesSuite extends FunSuite with Matchers {
  test("King can only attack one field from his current position") {
    val attacks = King.attacks(Board(3, 3), FieldIndex(4))
    attacks should have size 8
    attacks.map(_.index) should contain allOf(0, 1, 2, 3, 5, 6, 7, 8)

    val attacks1 = King.attacks(Board(7, 7), FieldIndex(32))
    attacks1 should have size 8
    attacks1.map(_.index) should contain allOf(24, 25, 26, 31, 33, 38, 39, 40)
  }

  test("King can attack only three fields when placed at the corner") {
    val attacks = King.attacks(Board(3, 3), FieldIndex(0))
    attacks should have size 3
    attacks.map(_.index) should contain allOf(1, 3, 4)

    val attacks1 = King.attacks(Board(7, 7), FieldIndex(48))
    attacks1 should have size 3
    attacks1.map(_.index) should contain allOf(40, 41, 47)
  }

  test("Queen should attack all fields on the lines and diagonals") {
    val attacks = Queen.attacks(Board(4, 4), FieldIndex(0))
    attacks should have size 9
    attacks.map(_.index) should contain allOf(4, 8, 1, 2, 3, 5, 10, 15)
  }

  test("Queen placed in the middle of the board should be able to attack multiple fields") {
    val attacks = Queen.attacks(Board(7, 7), FieldIndex(24))
    attacks should have size 24
    attacks.map(_.index) should contain allOf(
      3, 10, 17, 21, 22, 23, 25, 26, 27, 31, 38, 45, // Line moves
      0, 8, 16, 6, 12, 18, 30, 36, 42, 32, 40, 48 // Diagonal moves
    )
  }

  test("Rook should attack all fields on lines") {
    val attacks = Rook.attacks(Board(4, 4), FieldIndex(0))
    attacks should have size 6
    attacks.map(_.index) should contain allOf(1, 2, 3, 4, 8, 12)
  }

  test("Rook placed in the middle of the board should be able to attack multiple fields") {
    val attacks = Rook.attacks(Board(7, 7), FieldIndex(25))
    attacks should have size 12
    attacks.map(_.index) should contain allOf(4, 11, 18, 32, 39, 46, 21, 22, 23, 24, 26, 27)
  }

  test("Bishop should be able to attack only on diagonals") {
    val attacks = Bishop.attacks(Board(4, 4), FieldIndex(6))
    attacks should have size 5
    attacks.map(_.index) should contain allOf(1, 3, 11, 9, 12)
  }

  test("Bishop placed in the middle of the board should be able to attack multiple fields") {
    val attacks = Bishop.attacks(Board(7, 7), FieldIndex(31))
    attacks should have size 10
    attacks.map(_.index) should contain allOf(7, 15, 23, 39, 47, 13, 19, 25, 37, 43)
  }

  test("Knight should be able to attack in L shaped manner") {
    val attacks = Knight.attacks(Board(4, 4), FieldIndex(0))
    attacks should have size 2
    attacks.map(_.index) should contain allOf(6, 9)
  }

  test("Knight placed in the middle of the board should be able to attack exactly 8 fields") {
    val attacks = Knight.attacks(Board(7, 7), FieldIndex(24))
    attacks should have size 8
    attacks.map(_.index) should contain allOf(9, 11, 29, 33, 15, 19, 37, 39)
  }

  test("Pieces should display proper single character names after using toString() on them") {
    val pieces = Seq(King, Queen, Rook, Bishop)
    pieces.forall(piece => piece.toString == piece.getClass.getSimpleName.capitalize.head.toString) shouldEqual true
    Knight.toString shouldEqual "N"
  }
}
