package chess.pieces

import org.scalatest.{FunSuite, Matchers}

class PieceParserSuite extends FunSuite with Matchers {
  test("PieceParser should return Right(Piece object) when proper name provided") {
    PieceParser.parse("K") shouldEqual Right(King)
    PieceParser.parse("q") shouldEqual Right(Queen)
    PieceParser.parse("R") shouldEqual Right(Rook)
    PieceParser.parse("b") shouldEqual Right(Bishop)
    PieceParser.parse("N") shouldEqual Right(Knight)
  }

  test("PieceParser should return Left(error) when invalid name provided") {
    PieceParser.parse("x") shouldBe 'Left
  }
}
