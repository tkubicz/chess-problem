## Chess Problem

This application is an attempt to solve the [chess challenge](doc/chess_challenge.md).
It is written in plain scala using only two external libraries:

* scalatest - for unit and integration tests
* scopt - to simplify loading user input

### Building

To build this application use `sbt`.

In order bo build an executable jar use:

```sh
$ sbt assembly
```

The artifact should be available in this path: `/target/scala-2.12/chess-problem.jar`

It is also possible to use interactive mode of `sbt`:

```sh
$ sbt run
```

To run tests use:

```sh
$ sbt test
```

### Running

To run an executable jar use:

```sh
$ java -jar chess-problem.jar -w <int> -h <int> -p <piece>,<piece>... -r <true/false>
```

Available parameters are:
```
Usage: chess-problem [options]

  -w, --width <int>        Width of the chess board
  -h, --height <int>       Height of the chess board
  -p, --pieces <Piece>,<Piece>...
                           Possible values for <Piece>: K (King), Q (Queen), R (Rook), B (Bishop), N (Knight)
  -r, --results <bool>     Set this flag to 'true' to display all solutions on standard output. Default is 'false'
```

### Example usage

```sh
$ java -jar chess-problem.jar -w 3 -h 3 -p K,K,R -r true
Calculating solutions...
Solution 1:
  _   _   K
  R   _   _
  _   _   K

Solution 2:
  K   _   _
  _   _   R
  K   _   _

Solution 3:
  _   R   _
  _   _   _
  K   _   K

Solution 4:
  K   _   K
  _   _   _
  _   R   _

Number of solutions: 4
Solving the problem took: 88 ms
```

### Solution for 7x7 board

```sh
$ java -jar chess-problem.jar -w 7 -h 7 -p K,K,Q,Q,B,B,N
Calculating solutions...
Number of solutions: 3063828
Solving the problem took: 147791 ms
```